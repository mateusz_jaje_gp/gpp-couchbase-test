name := "gpp-couchbase-test"

version := "1.0"

scalaVersion := "2.11.8"

resolvers ++= Seq(
  "Typesafe Releases" at "http://repo.typesafe.com/typesafe/releases/",
  "Couchbase Maven Repository" at "http://files.couchbase.com/maven2",
  Resolver.url("Artifactory Realm Realeases", url("https://gpp-artifactory.gp-cloud.com/artifactory/gpp-libs-releases"))(Resolver.ivyStylePatterns),
  Resolver.url("Artifactory Realm Snapshots", url("https://gpp-artifactory.gp-cloud.com/artifactory/gpp-libs-snapshots"))(Resolver.ivyStylePatterns)
)

libraryDependencies ++= Seq(
  "com.google.code.gson" % "gson" % "2.2.4",
  "grandparade" %% "gpp-couchbase" % "0.98-SNAPSHOT",
  "org.joda" % "joda-convert" % "1.8"
)


initialCommands in console :=
  """
    |import test.Test
    |""".stripMargin