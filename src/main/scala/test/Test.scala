package test

import java.util.UUID

import akka.actor.ActorSystem
import akka.util.Timeout
import com.couchbase.client.java.document.RawJsonDocument
import com.couchbase.client.java.query.N1qlQuery
import com.gpplatform.commons.couchbase.connection.{ConfiguredCouchbaseManager, ManagedBucket}
import com.gpplatform.commons.couchbase.dataaccess.{EntityToRawJsonDocument, MinimalCouchbaseRepository}
import com.gpplatform.commons.domain.ObjectDoesNotExist
import com.gpplatform.commons.serializer.JodaDateTimeIsoSerializer
import com.twitter.bijection.Bijection
import com.twitter.bijection.Conversion.asMethod
import com.typesafe.config.Config
import org.joda.time.DateTime
import org.json4s.ext.JodaTimeSerializers
import org.json4s.{DefaultFormats, Formats}

import scala.concurrent.{Await, ExecutionContext, Future}

/**
  * Created by mateusz on 17.03.2016.
  */

case class User(id: UUID, name: String, surname: String, email: String, balance: Double, more: Map[String, String], createdAt: DateTime = DateTime.now())


trait UserRepository {

  def findByEmail(email: String): Future[User]

  def inserts(u: User): Future[User]
}

object User{
  type IdType = String
}

class UserCouchbaseRepository(bucket: ManagedBucket)(implicit ec: ExecutionContext)
  extends MinimalCouchbaseRepository[User, User.IdType](bucket)(ec)
    with UserRepository {

  implicit def json4sFormats: Formats = DefaultFormats ++ JodaTimeSerializers.all ++ JodaDateTimeIsoSerializer.dateTimeSerializers ++
    org.json4s.ext.JavaTypesSerializers.all


  override protected def documentKey(id: User.IdType): String = s"user:$id"

  override protected def entityId(entity: User): User.IdType = entity.email

  override protected implicit def entityToRawJsonDocument: Bijection[RawJsonDocument, User] =
    EntityToRawJsonDocument[User](entityId _ andThen documentKey)

  // ### methods from UserRepository

  def findByEmail(email: String): Future[User] =
    bucket.get(documentKey(email)).map(_.as[User])
      .recover { case e: NoSuchElementException => throw new ObjectDoesNotExist() }

  def inserts(u: User): Future[User] = {
    val document: RawJsonDocument = u.as[RawJsonDocument]
    println(s"$u is $document")
    bucket.insert(document).map { document1 =>
      val user: User = document1.as[User]
      println(s"$document1 is $user")
      user
    }
  }

  def remove(id: String) = {
    deleteById(id)
  }

  def all() = {
    import scala.collection.JavaConverters._
    bucket.sync.query(N1qlQuery.simple("SELECT * from users-test")).asScala
  }
}

object Test {

  import scala.concurrent.duration._

  System.setProperty("com.couchbase.queryEnabled", "true")

  implicit val actorSystem = ActorSystem("test")
  val config: Config = actorSystem.settings.config
  implicit val timeout = Timeout(60.seconds)

  implicit def executionContext = actorSystem.dispatcher

  val couchbase = ConfiguredCouchbaseManager.forActorSystem(actorSystem)
  val usersBucket = couchbase.getBucket("users-test")
  val usersRepo = new UserCouchbaseRepository(usersBucket)

  def New(email: String, name: String, surname: String, add: Map[String, String]) = {
    val now = System.currentTimeMillis()
    Await.result(usersRepo.inserts(User(UUID.randomUUID(), name, surname, email, 0d, add)), 50.seconds)
    println(s"inserting takes ${System.currentTimeMillis() - now}ms")
  }

  def find(email: String) = {
    val now = System.currentTimeMillis()
    val result = Await.result(usersRepo.findByEmail(email), 20.seconds)
    println(s"find takes ${System.currentTimeMillis() - now}ms")
    println(s"found user is $result")
  }

  def query() = {
    val now = System.currentTimeMillis()
    val result = usersRepo.all()
    println(s"find takes ${System.currentTimeMillis() - now}ms")
    println(s"found user is $result")
  }

  def clear() = {
    val result = Await.result(usersRepo.remove("user:a@a.a"), 10.seconds)
    println(s"removing result $result")
  }
}

/*
To test it run in command line
    $ sbt console

   and object Test will be imported do console scope, so you can run commands
   $> Test.find(some-email)
   $> Test.New(email,name,surname,Map(additional data))
   $> Test.clear()
   $> Test.query does not work at this moment

 */